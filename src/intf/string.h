#pragma once

#include <stddef.h>

size_t strlen(const char* str);
int strcmp(const char* s1, const char* s2);
char* itoa(int value, char* str, int base); 