#include "system.h"
#include "print.h"
#include <stdint.h>

static char cpu_brand[48];
static uint32_t total_memory = 0;

void init_system_info(multiboot_info_t* mb_info) {
    // Get CPU brand
    uint32_t *brand = (uint32_t*)cpu_brand;
    asm volatile ("cpuid" 
        : "=a"(brand[0]), "=b"(brand[1]), "=c"(brand[2]), "=d"(brand[3]) 
        : "a"(0x80000002));
    asm volatile ("cpuid" 
        : "=a"(brand[4]), "=b"(brand[5]), "=c"(brand[6]), "=d"(brand[7]) 
        : "a"(0x80000003));
    asm volatile ("cpuid" 
        : "=a"(brand[8]), "=b"(brand[9]), "=c"(brand[10]), "=d"(brand[11]) 
        : "a"(0x80000004));
    cpu_brand[48] = '\0';

    // Get memory from multiboot info
    if (mb_info->flags & (1 << 6)) {
        multiboot_memory_map_t* mmap = (multiboot_memory_map_t*)(uint64_t)mb_info->mmap_addr;
        while((uint64_t)mmap < (uint64_t)mb_info->mmap_addr + mb_info->mmap_length) {
            if (mmap->type == 1) {
                total_memory += (mmap->len) / 1024;
            }
            mmap = (multiboot_memory_map_t*)((uint64_t)mmap + mmap->size + sizeof(mmap->size));
        }
    }
}

const char* get_cpu_brand() {
    return cpu_brand;
}

uint32_t get_total_memory() {
    return total_memory;
} 